from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.shortcuts import render
from django.http import HttpResponse
from django.core.mail import send_mail, BadHeaderError
from django import forms


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/login/"
    template_name = "register.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/")


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=200)
    sender = forms.EmailField(max_length=350)
    message = forms.CharField(widget=forms.Textarea(attrs = {'class': 'form-control'}))
    copy = forms.BooleanField(required=False)



def cv(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            sender = form.cleaned_data['sender']
            message = form.cleaned_data['message']
            copy = form.cleaned_data['copy']

            recipients = ['handmade.noreply@gmail.com']
            if copy:
                recipients.append(sender)
            try:
                send_mail(subject, message, 'handmade.noreply@gmail.com', recipients)
            except BadHeaderError: #Защита от уязвимости
                return HttpResponse('Invalid header found')
            return render(request, 'thanks.html')
    else:
        form = ContactForm()
    return render(request, 'feedback.html', {'form': form})


